<?php
/**
*	TESTE VOOPTER
**/
?>
<!DOCTYPE html>
<html lang="pt-br">
<!-- HEAD -->
	<head>
	    <!-- MetaTags básicas -->
	    	<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
	    	<meta http-equiv="Content-Language" content="pt-br">
	    	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	    <!-- Fim das MetaTags básicas -->

	    <!-- Título -->
	    	<title>Voopter - o seu buscador inteligente de passagens áereas.</title>
	    <!-- Fim do Título -->

	    <!-- MetaTags de conteúdo -->
	    	<meta name="description" content="Voopter - o seu buscador inteligente de passagens áereas">
	    <!-- Fim das MetaTags de conteúdo -->

	    <!-- CSS -->
	    	<link rel="stylesheet" type="text/css" href="assets/css/voopter-lib.css">
	    	<link rel="stylesheet" type="text/css" href="assets/css/voopter-app.css">
	    <!-- Fim do CSS -->

	    <!-- JS -->
	    	<script src="assets/js/voopter-app.js" type="text/javascript"></script>
	    <!-- Fim do JS -->

	</head>
<!-- Fim do HEAD -->
<!-- BODY -->
	<body id="app">
		<section class="container">
			<div class="row"> 
				<div class="col-md-12">
					<!-- BANNER HEADER -->
					<div id="banner-header" class="text-center">
						<div id="logo-voopter" class="col-lg-4 offset-lg-4 mt-5">
							<img class="img-responsive" src="assets/img/png/logo-voopter.png" />
						</div>
						<div id="titulo-voopter" class="col-md-12 mt-4 mb-4">
							<h1 >
								Compare Preços de <span>PASSAGENS AÉREAS</span> de vários sites
							</h1>
						</div>
					</div>
					<!-- Fim do BANNER HEADER -->
					<!-- BANNER -->
					<div class="col-md-12">
						<iframe id="banner-buscador" src="iframe.html" scrolling="yes"></iframe>
					</div>
					<!-- Fim do BANNER -->
				</div>
			</div>
		</section>
	</body>
<!-- Fim do BODY -->
</html>


