/**
*	Módulos externos
**/
// jQuery [https://github.com/jquery/jquery]
window.$ = window.jQuery = require('jquery');

// Vue [https://github.com/vuejs/vue]
window.Vue = require('../../node_modules/vue/dist/vue.js');

// Tether [https://github.com/HubSpot/tether]
window.Tether = require('../../node_modules/tether/dist/js/tether.js');

// Tooltip [https://github.com/HubSpot/tooltip]
window.Tooltip = require('../../node_modules/tether-tooltip/dist/js/tooltip.js');

// Drop [https://github.com/HubSpot/drop]
window.Drop = require('../../node_modules/tether-drop/dist/js/drop.js');

// Boostrap [https://v4-alpha.getbootstrap.com]
require('../../node_modules/bootstrap/dist/js/bootstrap.js');

// Datepicker [https://github.com/uxsolutions/bootstrap-datepicker]
window.datepicker = require('../../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js');

// Select2 [https://github.com/select2/select2]
window.select2 = require('../../node_modules/select2/dist/js/select2.js');

// Smooth-Scroll [https://github.com/cferdinandi/smooth-scroll]
window.smoothScroll = require('../../node_modules/smooth-scroll/build/index.js');

// ScrollReveal [https://github.com/jlmakes/scrollreveal]
window.sr = require('../../node_modules/scrollreveal/dist/scrollreveal.js');

// SweetAlert 2 [https://github.com/limonte/sweetalert2]
window.swal = require('../../node_modules/sweetalert2/dist/sweetalert2.js');