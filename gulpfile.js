const gulp = require('gulp');
const gulpCopy = require('gulp-copy');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const concatCss = require('gulp-concat-css');
const watch = require('gulp-watch');
const browserify = require('gulp-browserify');

/** Para a pasta ASSETS **/
	gulp.task('copy-to-assets-jquery', function() {
		return gulp
		.src('./node_modules/jquery/dist/jquery.min.js')
		.pipe(gulpCopy('./assets/js', { prefix: 3 }));
	});

	gulp.task('copy-to-assets-fontawesome-fonts', function(){
		return gulp
		.src('./node_modules/font-awesome/fonts/*')
		.pipe(gulpCopy('./assets/fonts', { prefix: 3 }));
	});

/** Para a pasta RESOURCES **/
	gulp.task('copy-to-resources-bootstrap-css', function(){
		return gulp
		.src('./node_modules/bootstrap/dist/css/bootstrap.css')
		.pipe(gulpCopy('./resources/css', { prefix: 4 }));
	});

	gulp.task('copy-to-resources-bootstrap-datepicker-css', function(){
		return gulp
		.src('./node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css')
		.pipe(gulpCopy('./resources/css', { prefix: 4 }));
	});

	gulp.task('copy-to-resources-bootstrap-social-css', function(){
		return gulp
		.src('./node_modules/bootstrap-social/bootstrap-social.css')
		.pipe(gulpCopy('./resources/css', { prefix: 2 }));
	});

	gulp.task('copy-to-resources-fontawesome-css', function(){
		return gulp
		.src('./node_modules/font-awesome/css/font-awesome.css')
		.pipe(gulpCopy('./resources/css', { prefix: 4 }));
	});

	gulp.task('copy-to-resources-tether-css', function(){
		return gulp
		.src('./node_modules/tether/dist/css/tether.css')
		.pipe(gulpCopy('./resources/css', { prefix: 4 }));
	});

	gulp.task('copy-to-resources-tether-theme-css', function(){
		return gulp
		.src('./node_modules/tether/dist/css/tether-theme-basic.css')
		.pipe(gulpCopy('./resources/css', { prefix: 4 }));
	});

	gulp.task('copy-to-resources-tether-drop-css', function(){
		return gulp
		.src('./node_modules/tether-drop/dist/css/drop-theme-basic.css')
		.pipe(gulpCopy('./resources/css', { prefix: 4 }));
	});

	gulp.task('copy-to-resources-tether-tooltip-css', function(){
		return gulp
		.src('./node_modules/tether-tooltip/dist/css/tooltip-theme-arrows.css')
		.pipe(gulpCopy('./resources/css', { prefix: 4 }));
	});

	gulp.task('copy-to-resources-select2-css', function(){
		return gulp
		.src('./node_modules/select2/dist/css/select2.css')
		.pipe(gulpCopy('./resources/css', { prefix: 4 }));
	});

	gulp.task('copy-to-resources-sweetalert2-css', function(){
		return gulp
		.src('./node_modules/sweetalert2/dist/sweetalert2.css')
		.pipe(gulpCopy('./resources/css', { prefix: 3 }));
	});

/** VOOPTER SASS, compila os arquivos de estilização do Voopter pra RESOURCES **/
	gulp.task('sass', function(){
		return gulp
		.src('./resources/sass/voopter-app.scss')
		.pipe(sass().on('error', sass.logError))
	    .pipe(gulp.dest('./assets/css'));
	});

	gulp.task('sass-iframe', function(){
		return gulp
		.src('./resources/sass/voopter-iframe.scss')
		.pipe(sass().on('error', sass.logError))
	    .pipe(gulp.dest('./assets/css'));
	});

/** CONCAT CSS, tudo em um único arquivo CSS **/
	gulp.task('concat-css', function () {
	  return gulp
	  	.src('./resources/css/*.css')
	    .pipe(concatCss('voopter-lib.css'))
	    .pipe(gulp.dest('./assets/css/'));
	});

/** MINIFY CSS, tudo em um arquivo CSS mais leve **/
	gulp.task('compact-css', function() {
	  return gulp
	  	.src('./assets/css/*.css')
	    .pipe(cleanCSS({debug: true, compatibility: 'ie8'}, function(details) {
	        console.log(details.name + ': ' + details.stats.originalSize);
	        console.log(details.name + ': ' + details.stats.minifiedSize);
	    }))
	    .pipe(gulp.dest('./assets/css/'));
	});

/** BROWSERIFY **/
	gulp.task('browserify-lib', function() {
		return gulp
			.src('./resources/js/voopter-lib.js')
	        .pipe(browserify({
	          insertGlobals : true,
	          debug : false
	        }))
	        .pipe(gulp.dest('./assets/js'))
	});

	gulp.task('browserify-app', function() {
		return gulp
			.src('./resources/js/voopter-app.js')
	        .pipe(browserify({
	          insertGlobals : true,
	          debug : false
	        }))
	        .pipe(gulp.dest('./assets/js'))
	});

	gulp.task('browserify-iframe', function() {
		return gulp
			.src('./resources/js/voopter-iframe.js')
	        .pipe(browserify({
	          insertGlobals : true,
	          debug : false
	        }))
	        .pipe(gulp.dest('./assets/js'))
	});
